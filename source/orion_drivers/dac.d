/*
Object-oriented Linux drivers for D programming language.
Copyright (C) 2019 Orion Company.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module orion_drivers.dac;

import core.stdc.stdlib;
import core.stdc.string;
import core.stdc.errno;
import std.algorithm.searching : all;
import std.string;
import std.format;

private {
    enum snd_pcm_stream_t { SND_PCM_STREAM_PLAYBACK = 0, SND_PCM_STREAM_CAPTURE, SND_PCM_STREAM_LAST = SND_PCM_STREAM_CAPTURE }
    enum snd_pcm_access_t {
        SND_PCM_ACCESS_MMAP_INTERLEAVED = 0, SND_PCM_ACCESS_MMAP_NONINTERLEAVED, SND_PCM_ACCESS_MMAP_COMPLEX, SND_PCM_ACCESS_RW_INTERLEAVED,
        SND_PCM_ACCESS_RW_NONINTERLEAVED, SND_PCM_ACCESS_LAST = SND_PCM_ACCESS_RW_NONINTERLEAVED
    }
    enum snd_pcm_format_t {
        SND_PCM_FORMAT_UNKNOWN = -1, SND_PCM_FORMAT_S8 = 0, SND_PCM_FORMAT_U8, SND_PCM_FORMAT_S16_LE,
        SND_PCM_FORMAT_S16_BE, SND_PCM_FORMAT_U16_LE, SND_PCM_FORMAT_U16_BE, SND_PCM_FORMAT_S24_LE,
        SND_PCM_FORMAT_S24_BE, SND_PCM_FORMAT_U24_LE, SND_PCM_FORMAT_U24_BE, SND_PCM_FORMAT_S32_LE,
        SND_PCM_FORMAT_S32_BE, SND_PCM_FORMAT_U32_LE, SND_PCM_FORMAT_U32_BE, SND_PCM_FORMAT_FLOAT_LE,
        SND_PCM_FORMAT_FLOAT_BE, SND_PCM_FORMAT_FLOAT64_LE, SND_PCM_FORMAT_FLOAT64_BE, SND_PCM_FORMAT_IEC958_SUBFRAME_LE,
        SND_PCM_FORMAT_IEC958_SUBFRAME_BE, SND_PCM_FORMAT_MU_LAW, SND_PCM_FORMAT_A_LAW, SND_PCM_FORMAT_IMA_ADPCM,
        SND_PCM_FORMAT_MPEG, SND_PCM_FORMAT_GSM, SND_PCM_FORMAT_S20_LE, SND_PCM_FORMAT_S20_BE,
        SND_PCM_FORMAT_U20_LE, SND_PCM_FORMAT_U20_BE, SND_PCM_FORMAT_SPECIAL = 31, SND_PCM_FORMAT_S24_3LE = 32,
        SND_PCM_FORMAT_S24_3BE, SND_PCM_FORMAT_U24_3LE, SND_PCM_FORMAT_U24_3BE, SND_PCM_FORMAT_S20_3LE,
        SND_PCM_FORMAT_S20_3BE, SND_PCM_FORMAT_U20_3LE, SND_PCM_FORMAT_U20_3BE, SND_PCM_FORMAT_S18_3LE,
        SND_PCM_FORMAT_S18_3BE, SND_PCM_FORMAT_U18_3LE, SND_PCM_FORMAT_U18_3BE, SND_PCM_FORMAT_G723_24,
        SND_PCM_FORMAT_G723_24_1B, SND_PCM_FORMAT_G723_40, SND_PCM_FORMAT_G723_40_1B, SND_PCM_FORMAT_DSD_U8,
        SND_PCM_FORMAT_DSD_U16_LE, SND_PCM_FORMAT_DSD_U32_LE, SND_PCM_FORMAT_DSD_U16_BE, SND_PCM_FORMAT_DSD_U32_BE,
        SND_PCM_FORMAT_LAST = SND_PCM_FORMAT_DSD_U32_BE, SND_PCM_FORMAT_S16 = SND_PCM_FORMAT_S16_LE, SND_PCM_FORMAT_U16 = SND_PCM_FORMAT_U16_LE, SND_PCM_FORMAT_S24 = SND_PCM_FORMAT_S24_LE,
        SND_PCM_FORMAT_U24 = SND_PCM_FORMAT_U24_LE, SND_PCM_FORMAT_S32 = SND_PCM_FORMAT_S32_LE, SND_PCM_FORMAT_U32 = SND_PCM_FORMAT_U32_LE, SND_PCM_FORMAT_FLOAT = SND_PCM_FORMAT_FLOAT_LE,
        SND_PCM_FORMAT_FLOAT64 = SND_PCM_FORMAT_FLOAT64_LE, SND_PCM_FORMAT_IEC958_SUBFRAME = SND_PCM_FORMAT_IEC958_SUBFRAME_LE, SND_PCM_FORMAT_S20 = SND_PCM_FORMAT_S20_LE, SND_PCM_FORMAT_U20 = SND_PCM_FORMAT_U20_LE
    }
    enum snd_pcm_state_t {
        SND_PCM_STATE_OPEN = 0, SND_PCM_STATE_SETUP, SND_PCM_STATE_PREPARED, SND_PCM_STATE_RUNNING,
        SND_PCM_STATE_XRUN, SND_PCM_STATE_DRAINING, SND_PCM_STATE_PAUSED, SND_PCM_STATE_SUSPENDED,
        SND_PCM_STATE_DISCONNECTED, SND_PCM_STATE_LAST = SND_PCM_STATE_DISCONNECTED, SND_PCM_STATE_PRIVATE1 = 1024
    }

    alias snd_pcm_sframes_t = long;
    alias snd_pcm_uframes_t = ulong;

    struct snd_pcm_channel_area_t {
        void* addr;
        uint first;
        uint step;
    }

    struct snd_pcm_t {}
    struct snd_pcm_hw_params_t {}
    struct snd_pcm_sw_params_t {}
    struct snd_async_handler_t {}

    alias snd_async_callback_t = extern(C) void function(snd_async_handler_t *handler);
}

private extern(C) {
    int snd_pcm_open(snd_pcm_t** pcmp, const(char*) name, snd_pcm_stream_t stream, int mode);
    int snd_pcm_close(snd_pcm_t* pcm);
    int snd_pcm_drain(snd_pcm_t* pcm);
    int snd_pcm_drop(snd_pcm_t* pcm);
    int snd_pcm_prepare(snd_pcm_t* pcm);
    int snd_pcm_start(snd_pcm_t* pcm);
    const(char*) snd_strerror(int errnum);

    int snd_pcm_hw_params(snd_pcm_t* pcm, snd_pcm_hw_params_t* params);
    int snd_pcm_hw_params_any(snd_pcm_t *pcm, snd_pcm_hw_params_t* params);
    int snd_pcm_hw_params_set_access(snd_pcm_t* pcm, snd_pcm_hw_params_t *params, snd_pcm_access_t _access);
    int snd_pcm_hw_params_set_format(snd_pcm_t* pcm, snd_pcm_hw_params_t *params, snd_pcm_format_t val);
    int snd_pcm_hw_params_set_channels(snd_pcm_t* pcm, snd_pcm_hw_params_t* params, uint val);
    int snd_pcm_hw_params_set_rate_near(snd_pcm_t* pcm, snd_pcm_hw_params_t* params, uint* val, int* dir);
    int snd_pcm_hw_params_set_period_size_near(snd_pcm_t* pcm, snd_pcm_hw_params_t* params, snd_pcm_uframes_t* val, int* dir);
    int snd_pcm_hw_params_get_period_size(const snd_pcm_hw_params_t *params, snd_pcm_uframes_t *frames, int *dir);

    int snd_pcm_sw_params (snd_pcm_t *pcm, snd_pcm_sw_params_t *params);
    int snd_pcm_sw_params_malloc(snd_pcm_sw_params_t** ptr);
    int snd_pcm_sw_params_current(snd_pcm_t* pcm, snd_pcm_sw_params_t* params);
    int snd_pcm_sw_params_set_start_threshold(snd_pcm_t* pcm, snd_pcm_sw_params_t* params, snd_pcm_uframes_t val);
    int snd_pcm_sw_params_set_avail_min(snd_pcm_t* pcm, snd_pcm_sw_params_t* params, snd_pcm_uframes_t val);

    int snd_pcm_format_physical_width(snd_pcm_format_t format);

    int snd_async_add_pcm_handler(snd_async_handler_t** handler, snd_pcm_t* pcm, snd_async_callback_t callback, void* private_data);
    snd_pcm_t* snd_async_handler_get_pcm(snd_async_handler_t* handler);
    void* snd_async_handler_get_callback_private(snd_async_handler_t* handler);
    snd_pcm_sframes_t snd_pcm_avail_update(snd_pcm_t* pcm);
    snd_pcm_state_t snd_pcm_state(snd_pcm_t* pcm);

    snd_pcm_sframes_t snd_pcm_writei(snd_pcm_t* pcm, const void* buffer, snd_pcm_uframes_t size);
    //int snd_pcm_pause(snd_pcm_t* pcm, int enable);

    size_t 	snd_pcm_hw_params_sizeof();
    //void my_snd_pcm_hw_params_alloca(snd_pcm_hw_params_t** ptr);
}

static if(false) {
    private void snd_alloca(Type)(Type** ptr) {
        *ptr = cast (Type*)alloca(Type.sizeof); // sizeof returns a wrong value.
        memset(*ptr, 0, Type.sizeof);
    }
}

//alias snd_pcm_hw_params_alloca = snd_alloca!snd_pcm_hw_params_t;

class AnalogSignalException : Exception {
    this(string msg, string file = __FILE__, size_t line = __LINE__) {
        super(msg, file, line);
    }
}

class SoundException : AnalogSignalException {
    this(string msg, string file = __FILE__, size_t line = __LINE__) {
        super(msg, file, line);
    }
    static SoundException fromSndError(int rc, string str) {
        return new SoundException(format(str ~ ": %s", snd_strerror(rc).fromStringz));
    }
    static void checkSndError(int rc, string str) {
        if(rc < 0) throw fromSndError(rc, str);
    }
}

interface DAC {
   void setVoltage(float value);
}

// An alternative solution would be to change the AC volatage using mixer, but:
// - Who knows if mixer is linear or whatever.
// - If we use mixer, should we set front, rear, both, etc.?
// TODO: ALSA API is not thread safe, i.e., calls for one PCM device must all be from one thread, or synchronized.
class SoundCard {
    private snd_pcm_t *handle = null;
    static immutable numberOfChannels = 2; // stereo
    private uint divider = 1;
    private snd_pcm_uframes_t period_size;
    private short[] levels; // remember levels for every channel
    private short[] buffer;

    this(const string deviceName = "default") {
        immutable rc = snd_pcm_open(&handle, deviceName.toStringz, snd_pcm_stream_t.SND_PCM_STREAM_PLAYBACK, 0);
        SoundException.checkSndError(rc, "Unable to open pcm device");

        setupItAsDAC();
    }
    ~this() {
        if(!handle)
            return;
        {
            immutable rc = snd_pcm_drain(handle);
            assert(rc == 0, format("Unable to drain pcm device: %s", snd_strerror(rc).fromStringz));
        }
        {
            immutable rc = snd_pcm_close(handle);
            assert(rc == 0, format("Unable to close pcm device: %s", snd_strerror(rc).fromStringz));
        }
    }
    private void setupItAsDAC() { /// Silly setup to use the card as DAC.
        snd_pcm_hw_params_t* params;
        params = cast(snd_pcm_hw_params_t*) alloca(snd_pcm_hw_params_sizeof());
        memset(params, 0, snd_pcm_hw_params_sizeof());
        snd_pcm_hw_params_any(handle, params);

        {
            immutable rc = snd_pcm_hw_params_set_access( handle, params, snd_pcm_access_t.SND_PCM_ACCESS_RW_INTERLEAVED);
            SoundException.checkSndError( rc, "Unable to set access for hw parameters");
        }

        // Signed 16 bit CPU endian.
        snd_pcm_hw_params_set_format( handle, params, snd_pcm_format_t.SND_PCM_FORMAT_S16);

        snd_pcm_hw_params_set_channels( handle, params, numberOfChannels);

        /* 44100 bits/second sampling rate (CD quality) */
        {
            //uint val = 44100;
            uint val = 1000;
            snd_pcm_hw_params_set_rate_near( handle, params, &val, null); // FIXME: Here and in other places error checking
        }

        //{
        //    uint val;
        //    snd_pcm_hw_params_get_rate(params, &val, null);
        //    import std.stdio; writeln(val);
        //}

        {
            snd_pcm_uframes_t frames = 20000; // 2 // FIXME
            snd_pcm_hw_params_set_period_size_near(handle, params, &frames, null);
        }

        /* Write the parameters to the driver */
        {
            immutable rc = snd_pcm_hw_params( handle, params);
            SoundException.checkSndError( rc, "Unable to set hw parameters");
        }

        /* Use a buffer large enough to hold one period */
        {
            int dir;
            immutable rc = snd_pcm_hw_params_get_period_size(params, &period_size, &dir);
            SoundException.checkSndError(rc, "Unable to get pcm period size");
        }
        immutable size = period_size * numberOfChannels;
        buffer = new short[size];
        levels = new short[numberOfChannels]; // one element for plus and one for minus
        foreach(ref e; levels) e = 0;

        snd_pcm_sw_params_t *sw_params;
        {
            immutable rc = snd_pcm_sw_params_malloc(&sw_params); // FIXME: dellocate?
            SoundException.checkSndError(rc, "Cannot allocate software parameters structure");
        }
        {
            immutable rc = snd_pcm_sw_params_current(handle, sw_params);
            SoundException.checkSndError(rc, "Cannot initialize software parameters structure");
        }
        {
            // https://ferryzhou.wordpress.com/2012/02/23/alsa-broken-pipe/
            // FIXME
            //immutable rc = snd_pcm_sw_params_set_start_threshold( handle, sw_params, (size / period_size) * period_size);
            immutable rc = snd_pcm_sw_params_set_start_threshold( handle, sw_params, 3);
            SoundException.checkSndError( rc, "Unable to set start threshold");
        }
        {
            immutable rc = snd_pcm_sw_params_set_avail_min(handle, sw_params, 32);
            SoundException.checkSndError( rc, "Cannot set avail min");
        }
        {
            immutable rc = snd_pcm_sw_params(handle, sw_params);
            SoundException.checkSndError( rc, "Cannot set software parameters");
        }

        // No need
        //{
        //    immutable rc = snd_pcm_prepare(handle); // FIXME: also on underrun
        //    SoundException.checkSndError(rc, "Unable to prepare pcm");
        //}

        setupAsyncWork();
    }
    // https://www.alsa-project.org/alsa-doc/alsa-lib/_2test_2pcm_8c-example.html
    private void setupAsyncWork() {
        snd_async_handler_t *ahandler;
        {
            immutable rc = snd_async_add_pcm_handler(&ahandler, handle, &async_callback, cast(void*) this);
            SoundException.checkSndError(rc, "Unable to register async handler");
        }
    }
    //private snd_pcm_channel_area_t *areas;
    private uint offset = 0; // TODO: What is the best type?
    private bool phase = false;
    private uint phaseCount = 0;
    static extern(C) void async_callback(snd_async_handler_t* ahandler)
    {
        //import std.stdio; writeln("callback");
        snd_pcm_t *handle = snd_async_handler_get_pcm(ahandler);
        SoundCard object = cast(SoundCard) snd_async_handler_get_callback_private(ahandler);
        //snd_pcm_channel_area_t *areas = data.areas;
        snd_pcm_sframes_t avail = snd_pcm_avail_update(handle);
        while (avail >= 1/*object.period_size*/) {
            //writeln(avail);
            //object.generate_sine(object.period_size, object.phase); // FIXME
            immutable rc = snd_pcm_writei(handle, &object.buffer[object.offset], object.period_size);
            SoundException.checkSndError(cast (int)rc, "Write error");
            if(object.offset += (object.period_size * numberOfChannels) >= object.buffer.length) object.offset = 0;
            if (rc != object.period_size) // FIXME
                throw new SoundException(format("Write error: written %d expected %d\n", rc, object.period_size));
            avail = snd_pcm_avail_update(handle);
        }
    }
    // TODO: No need to generate a long sample, use short and write by parts
    void generate_sine(snd_pcm_uframes_t count, ref bool phase)
    {
        auto ptr = &buffer[0];
        ushort levelNumber = 0;
        foreach(i; 0..period_size) {
            assert(ptr < &buffer[0] + buffer.length);
            *ptr++ = phase ? levels[levelNumber++] : cast (short)-cast (int)levels[levelNumber++]; // TODO: more clean code
            if(levelNumber == numberOfChannels) {
                levelNumber = 0;
                if(++phaseCount == divider) {
                    phase = !phase;
                    phaseCount = 0;
                }
            }
        }
    }
    void setLevels(float[] newLevels)
    in {
        assert(newLevels.length == numberOfChannels, "Wrong number of channels");
    }
    do {
        // Calculate and store signal levels:
        foreach(i; 0..numberOfChannels) {
            auto v = newLevels[i] * 32768;
            if(v > 32637)
                v = 32637;
            else if(v < 0)
                v = 0;
            levels[i] = cast (short)(v);
        }

        startPlaying();
    }
    void startPlaying() {
        generate_sine(period_size, phase);

        // https://alsa.opensrc.org/Asynchronous_Playback_(Howto):
        // Two times the period size seems to be a good initial write size? Going below this results in a buffer underrun
        // (bro
        // ken pipe error on any subsequent function calls) and going over it seems to trigger "file descriptor in a bad state" errors.
        foreach(i; 0..2) {
            immutable rc = snd_pcm_writei(handle, &buffer[0], period_size);
            SoundException.checkSndError(cast (int)rc, "Initial write error");
            if (rc != period_size) break;
                //throw new SoundException(format("Initial write error: written %d expected %d", rc, period_size));
        }
        {
            //immutable rc = snd_pcm_drop( handle);
            //SoundException.checkSndError( rc, "Unable to drop pcm device signal");
        }
        if (snd_pcm_state(handle) == snd_pcm_state_t.SND_PCM_STATE_PREPARED) { // TODO: need to check?
            immutable rc = snd_pcm_start(handle);
            SoundException.checkSndError(rc, "Start error");
        }
    }
    /// No warranty that it will work unless called directly after the constructor.
    void setDivider(uint value) {
        divider = value;
    }
}
