module sound_test;

import core.thread;
import std.typecons;
import std.conv;
import orion_drivers.dac;

void main(string[] args) {
    uint pause = 1000;
    if(args.length > 1)
        pause = to!uint(args[1]);

    // "default" async control does not work under PulseAudio
    auto sc = scoped!SoundCard("hw:0");
    sc.setDivider(10);
    while(true) {
        sc.setLevels([0.15, 0.15]);
        Thread.sleep(dur!("msecs")(pause));
        sc.setLevels([0.00, 0.00]);
        Thread.sleep(dur!("msecs")(pause));
    }
}
