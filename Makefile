#!/usr/bin/make -f

.PHONY: all
all: drivers_sound-test

.PHONY: libdrivers_library.a
libdrivers_library.a:
	#make source/orion_drivers/dac_c.o
	dub build drivers:library --config=static-library

.PHONY: libdrivers_library.so
libdrivers_library.so:
	dub build drivers:library --config=dynamic-library

drivers_sound-test: libdrivers_library.a
	dub build drivers:sound-test

.PHONY: clean
clean:
	dub clean
	rm -rf lib classes drivers_sound-test
	find -name "*.[ao]" -o -name "*.so" | xargs rm -f